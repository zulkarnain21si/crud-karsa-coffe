@extends('default')

@section('content')

	@if($errors->any())
		<div class="alert alert-danger">
			@foreach ($errors->all() as $error)
				{{ $error }} <br>
			@endforeach
		</div>
	@endif

	{!! Form::open(['route' => 'bahanbakus.store']) !!}

		<div class="mb-3">
			{{ Form::label('nama', 'Nama', ['class'=>'form-label']) }}
			{{ Form::text('nama', null, array('class' => 'form-control')) }}
		</div>
		<div class="mb-3">
			{{ Form::label('stok', 'Stok', ['class'=>'form-label']) }}
			{{ Form::text('stok', null, array('class' => 'form-control')) }}
		</div>
		<div class="mb-3">
			{{ Form::label('satuan', 'Satuan', ['class'=>'form-label']) }}
			{{ Form::text('satuan', null, array('class' => 'form-control')) }}
		</div>
		<div class="mb-3">
			{{ Form::label('supplier', 'Supplier', ['class'=>'form-label']) }}
			{{ Form::text('supplier', null, array('class' => 'form-control')) }}
		</div>


		{{ Form::submit('Create', array('class' => 'btn btn-primary')) }}

	{{ Form::close() }}


@stop